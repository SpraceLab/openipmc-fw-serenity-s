
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "terminal.h"
#include "mt_printf.h"


/*
 * Multitask version for the original CLI_GetIntState() provided by terminal.
 *
 * Due to the multitask approach of this project, this function must be used
 * in the callbacks to test if ESC was pressed.
 */
extern bool mt_CLI_GetIntState();



static uint8_t sensors_cb()
{
	uint8_t tx_dataH, tx_dataL,  rx_dataH, rx_dataL;
	uint16_t value;

	tx_dataH = 0x01; //MCP9902 - Ext temp high part
	tx_dataL = 0x10; //MCP9902 - Ext temp low part
	sense_i2c_transmit( 0xf8, &tx_dataH, 1, 100 );
	sense_i2c_receive( 0xf8, &rx_dataH, 1, 100 );
	sense_i2c_transmit( 0xf8, &tx_dataL, 1, 100 );
	sense_i2c_receive( 0xf8, &rx_dataL, 1, 100 );

	value = ((rx_dataH<<3) + (rx_dataL>>5))*0.125 ;

	mt_printf( "X0 temp sensor data (0xf8) C %d\r\n", value );

	sense_i2c_transmit( 0x98, &tx_dataH, 1, 100 );
	sense_i2c_receive( 0x98, &rx_dataH, 1, 100 );
	sense_i2c_transmit( 0x98, &tx_dataL, 1, 100 );
	sense_i2c_receive( 0x98, &rx_dataL, 1, 100 );

	value = ((rx_dataH<<3) + (rx_dataL>>5))*0.125 ;

	mt_printf( "X1 temp sensor data (0x98) C %d\r\n", value );


	return TE_OK;
}


/*
 * This functions is called during terminal initialization to add custom
 * commands to the CLI by using CLI_AddCmd functions.
 *
 * Use the CLI_AddCmd() function according terminal documentation
 */
void add_board_specific_terminal_commands( void )
{
	CLI_AddCmd( "sensors", sensors_cb, 0, TMC_None, "Show X0 and X1 readings." );
}
