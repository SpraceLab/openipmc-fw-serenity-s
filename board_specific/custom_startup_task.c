#include <stdio.h>

#include "cmsis_os.h"
#include "sense_i2c.h"
#include "dimm_gpios.h"


extern void openipmc_start( void );
extern void set_benchtop_payload_power_level( uint8_t new_power_level );

/*
 * This task is launched by OpenIPMC-FW to allow user to run their own code or
 * launch any other desired task.
 * 
 * The priority and stack size allocated for this task can be controlled in
 * custom_settings.h
 */
void custom_startup_task( void *pvParameters )
{
	/*
	 * User can choose when the OpenIPMC is started. If any OpenIPMC
	 * board-specific hook/callback requires some previous code to run,
	 * it must be done before this point.
	 */
	openipmc_start();
	
	uint8_t blinkrate = 4;
	uint8_t counter = 0;

	for(;;)
	{
		// Blink led
		vTaskDelay( (125*blinkrate)/portTICK_PERIOD_MS );
		LED_2_SET_STATE(SET);
		vTaskDelay( (125*blinkrate)/portTICK_PERIOD_MS );
		LED_2_SET_STATE(RESET);
		
		uint8_t tx_dataH, tx_dataL,  rx_dataH, rx_dataL;
		uint16_t valueX0, valueX1;
		
		tx_dataH = 0x01; //MCP9902 - Ext temp high part
		tx_dataL = 0x10; //MCP9902 - Ext temp low part
		uint32_t status1 = sense_i2c_transmit( 0xf8, &tx_dataH, 1, 100 );
		uint32_t status2 = sense_i2c_receive( 0xf8, &rx_dataH, 1, 100 );
		sense_i2c_transmit( 0xf8, &tx_dataL, 1, 100 );
		sense_i2c_receive( 0xf8, &rx_dataL, 1, 100 );
		
		valueX0 = ((rx_dataH<<3) + (rx_dataL>>5))*0.125 ;
		
		sense_i2c_transmit( 0x98, &tx_dataH, 1, 100 );
		sense_i2c_receive( 0x98, &rx_dataH, 1, 100 );
		sense_i2c_transmit( 0x98, &tx_dataL, 1, 100 );
		sense_i2c_receive( 0x98, &rx_dataL, 1, 100 );
		
		valueX1 = ((rx_dataH<<3) + (rx_dataL>>5))*0.125 ;
		
		char message[25];
		if (status1==HAL_I2C_ERROR_NONE && status2==HAL_I2C_ERROR_NONE) {
			sprintf(message,"OK,X0 temp %d",valueX0);
		} else {
			sprintf(message,"ERR,X0 temp %d",valueX0);
		}
		
		
		//if (valueX0>90 && valueX0<150) {
		if ((valueX0>90 && status1==HAL_I2C_ERROR_NONE && status2==HAL_I2C_ERROR_NONE) || (valueX1>90 && valueX1<128)) {
			counter += 1;
		} else {
			counter = 0;
		}
	
		if (counter>3)  {
			set_benchtop_payload_power_level( 0 );
			blinkrate = 1;
		}


	}

}
