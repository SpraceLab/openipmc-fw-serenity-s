# OpenIPMC-FW for Serenity

This is a dedicated Intelligent Platform Management Control (IPMC) for Serenity ATCA boards, based on OpenIPMC-FW (firmware framework) to run on OpenIPMC-HW (miniDIMM mezzanine).



## Clone and Build

```
git clone --recurse-submodules git@gitlab.com:SpraceLab/openipmc-fw-serenity.git
make
```

The output files files `openipmc-fw_CM7.bin`and `upgrade.hpm` are copied into the project root.

This project is built automatically by GitLab via CI script. The outputs can be found in *artifacts*.

## IPMI Remote Upgrade via IPMI

The remote upgrade via IPMI can be done by using IPMItool and the `upgrade.hpm` file generated when this project is built via `make`.

Upload the new FW and then activate it by using this 2 commands:

```console
$ ipmitool -H x.x.x.x -P "" -t 0x86 hpm upgrade upgrade.hpm force
$ ipmitool -H x.x.x.x -P "" -t 0x86 hpm activate
```

where *x.x.x.x* is the IP address of the ShelfManager and *0x86* is the IPMI address of the board to be upgraded.

Since this project is built automatically by GitLab via CI script, the `upgrade.hpm` can be downloaded from the *artifacts*.

**NOTE:** The remote update requires the Bootloader to be installed into the MCU. Get it __[here](https://gitlab.com/openipmc/openipmc-fw-bootloader)__.

## Obsolete branches

The following branches are no longer being maintained after canging to the *submodule* approach:

- `master_fork`: It was used to follow the `master` branch from __[OpenIPMC-FW](gitlab.com/openipmc/openipmc-fw)__ when this project started as a fork of that one (*fork* approach). Originally named `master`, this branch was renamed when changing to *submodule* approach.

- `serenity1.0`: Used to keep the dedicated changes for the IPMC used in Serenity 1.0

- `Serenity1.2`: Used to keep the dedicated changes for the IPMC used in Serenity 1.2
